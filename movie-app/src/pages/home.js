import React from 'react';
import HomeCarousel from '../components/HomeCarousel';
import Footer from '../components/Footer';

function Home() {
    return (
        <div>
            <HomeCarousel />
            <Footer />
        </div>
    )
}

export default Home
