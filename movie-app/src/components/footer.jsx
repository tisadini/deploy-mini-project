import React from 'react';
import '../pages/Home.css';


function Footer() {
    return (
    <div className="footer">
      <div className="overlap-group">
        <div className="flex-row-2">
          <div className="flex-row-1">
            <div className="flex-col-2">
              <div className="footer-brand">
                <div className="brand-logo">
                  <img
                    className="polygon-1"
                    src="polygon-1.svg"
                  />
                </div>
                <h1 className="title roboto-medium-white-64px">MilanTV</h1>
              </div>
              <div className="text-1 roboto-light-white-30px">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                industry&#39;s standard.printing and typesetting industry. Lorem Ipsum has been the industry&#39;s
                standard
              </div>
            </div>
            <div className="flex-col-4">
              <div className="tentang-kami roboto-light-white-36px">Tentang kami</div>
              <div className="blog roboto-light-white-36px">Blog</div>
              <div className="layanan roboto-light-white-36px">Layanan</div>
              <div className="karir roboto-light-white-36px">Karir</div>
              <div className="pusat-media roboto-light-white-36px">Pusat Media</div>
            </div>
          </div>
          <div className="flex-col-1">
            <div className="flex-col">
              <div className="download roboto-medium-white-36px">Download</div>
              <div className="flex-row-3">
                <img
                  className="google-play-1"
                  src="google-play-1.png"
                />
                <img
                  className="apple-store-1"
                  src="apple-store-1.png"
                />
              </div>
              <div className="social-media roboto-medium-white-36px">Social media</div>
            </div>
            <div className="flex-row">
              <img
                className="face-1"
                src="face-1.png"
              />
              <img
                className="pinterest-1"
                src="pinterest-1.png"
              />
              <img
                className="instagram-1"
                src="instagram-1.png"
              />
            </div>
          </div>
        </div>
        <div className="flex-col-3">
          <img
            className="line-1"
            src="line-1.svg"
          />
          <div className="text-2 roboto-regular-normal-white-36px">
            Copyright © 2000-202 MilanTV.&nbsp;&nbsp;All Rights Reserved
          </div>
        </div>
      </div>
    </div>
  );
}

export default Footer;
